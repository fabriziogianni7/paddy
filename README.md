PADDYCOIN (PADDY) ICO
It is a simple ICO for a new token called PADDY, used for padel creeps.

token contract is in contracts folder
migrations is in migration folder
test is in test folder

the contract is deployed on ropsten, at the address 0xAb5e9c857b5747D38138CC4Ac2788aA87Bd5FfD4
solidity version 0.8.0
all contracts deployed are upgradable

to run the UI run (node version: 10.24.0)
cd client
npm install
npm run start

to run with docker:
cd client
docker run -it -p 3000:3000 client  


I used openzeppelin v.4 and create-react-app to develop the application
repo location: https://gitlab.com/fabriziogianni7/paddy



