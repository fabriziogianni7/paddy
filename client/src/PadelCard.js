import React, { Component } from 'react';
import { Card } from 'react-bootstrap';


import styles from './App.module.scss';


class PadelCard extends Component {

	render() {
		return (
			<Card className={styles.PadelCard}>
				<Card.Img id={styles.PadelPic} src={require("./images/padel.jpg")} />
				<Card.Body>
					<Card.Text as="h1">
						PaddyCoin (PADDY) ICO.
					</Card.Text>
					<Card.Text as="h2">
						Let's build a decentralized market for padel creeps.
					</Card.Text>
				</Card.Body>
			</Card>

		);
	}
}


export default PadelCard;