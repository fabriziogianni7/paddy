import React, { Component } from 'react';
import { Navbar } from 'react-bootstrap';

import Web3 from 'web3';
import PaddyJson from '../contracts/Paddy.json';

import styles from './App.module.scss';

const INFURA_ID = "69ab2c89571f422f842756d05067ee59";
const PADDY_ADDRESS = '0xAb5e9c857b5747D38138CC4Ac2788aA87Bd5FfD4';

class Bar extends Component {
	constructor(props) {
		super(props);
		this.web3 = new Web3(new Web3.providers.HttpProvider(`https://ropsten.infura.io/v3/${INFURA_ID}`));
		this.contract = new this.web3.eth.Contract(PaddyJson.abi, PADDY_ADDRESS);
		this.paddyBalance();
		
		this.contract.events.Bought((err,res)=> {
			if(!err)
				this.paddyBalance();
			console.log(err);
		});
		this.paddyBalance = this.paddyBalance.bind(this);

		this.state = {
			balance :""
		}
	}

	async componentDidMount(){
		this.paddyBalance();
	}

	async paddyBalance() {
		try {
			const bal = await this.contract.methods.getPaddyBalance().call();
			this.setState({balance : this.web3.utils.fromWei(bal,"ether")})
			
		} catch (error) {
			console.log(error);
			this.setState({balance : `∞`})
		}
	}

	render() {
		return (
			<Navbar expand="lg" fixed="top" className={styles.Bar}>
				<Navbar.Text id={styles.BarBrand}>PaddyCoin (PADDY) </Navbar.Text>
				<Navbar.Text id={styles.BarBalance}>PADDY market value: {this.state.balance} ETH</Navbar.Text>
				<Navbar.Text id={styles.BarName}><a id={styles.name} target="_blank" rel="noopener noreferrer"  title="Head to my linkedin" href="https://www.linkedin.com/in/fabrizio-giannitrapani/?locale=en_US">by Fabriziogianni7</a></Navbar.Text>
			</Navbar>

		);
	}
}


export default Bar;