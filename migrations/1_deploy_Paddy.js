const  { deployProxy} = require('@openzeppelin/truffle-upgrades');

const Paddy = artifacts.require('Paddy');

module.exports = async function (deployer) {
  try {
    const paddy = await deployProxy(Paddy, {deployer});
    console.log(`${await paddy.contract.methods.name().call()} deployed at ${paddy.address}`);
    
  } catch (error) {
    console.error(error);
  }

};