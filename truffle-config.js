const secret = require("./secret.json")
const HDWalletProvider = require("truffle-hdwallet-provider");
// Create your own key for Production environments (https://infura.io/)
const INFURA_ID = secret.INFURA_ID;
const MNEMONIC = secret.MNEMONIC;
// 
const configNetwok = (network, network_id, path = "m/44'/60'/0'/0/", gas = 4465030, gasPrice = 1e10) => ({
  provider: () => new HDWalletProvider(
    MNEMONIC, `https://${network}.infura.io/v3/${INFURA_ID}`, 
        0, 1, true, path
    ),
  network_id,
  gas,
  gasPrice,
});

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
    },
    ropsten: configNetwok('ropsten', 3),
    // kovan: configNetwok('kovan', 42),
    // rinkeby: configNetwok('rinkeby', 4),
    // main: configNetwok('mainnet', 1),
  },
  compilers: {
    solc: {
       version: "^0.8.0"
     }
   },
};
