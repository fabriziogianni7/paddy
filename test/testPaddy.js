const { assert } = require("chai");
const { expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const Paddy = artifacts.require("Paddy");

contract("Paddy", async accounts => {
    let instance;

    it("contract deployment", async () => {
        instance = await Paddy.deployed();
        const name = await instance.name.call();
        const symbol = await instance.symbol.call();
        assert.equal(name, "PaddyCoin")
        assert.equal(symbol, "PADDY")
    });

    it("buy token", async () => {
        const transaction = await instance.buy.sendTransaction({
            from: accounts[0],
            value: "10"
        });
        await expectEvent(transaction,"Bought")
        assert.equal(transaction.receipt.status, true);
        const bal = await instance.getPaddyBalance.call();
        assert.equal(bal.toNumber(), 10);
    });

    it("revert if transaction value is 0", async () => {
        const transaction = instance.buy.sendTransaction({
            from: accounts[0],
            value: "0"
        });
        
        await expectRevert.unspecified(transaction)
    });

    //   it("should call a function that depends on a linked library", async () => {
    //     const meta = await Paddy.deployed();
    //     const outCoinBalance = await meta.getBalance.call(accounts[0]);
    //     const metaCoinBalance = outCoinBalance.toNumber();
    //     const outCoinBalanceEth = await meta.getBalanceInEth.call(accounts[0]);
    //     const metaCoinEthBalance = outCoinBalanceEth.toNumber();
    //     assert.equal(metaCoinEthBalance, 2 * metaCoinBalance);
    //   });

    //   it("should send coin correctly", async () => {
    //     // Get initial balances of first and second account.
    //     const account_one = accounts[0];
    //     const account_two = accounts[1];

    //     const amount = 10;

    //     const instance = await Paddy.deployed();
    //     const meta = instance;

    //     const balance = await meta.getBalance.call(account_one);
    //     const account_one_starting_balance = balance.toNumber();

    //     balance = await meta.getBalance.call(account_two);
    //     const account_two_starting_balance = balance.toNumber();
    //     await meta.sendCoin(account_two, amount, { from: account_one });

    //     balance = await meta.getBalance.call(account_one);
    //     const account_one_ending_balance = balance.toNumber();

    //     balance = await meta.getBalance.call(account_two);
    //     const account_two_ending_balance = balance.toNumber();

    //     assert.equal(
    //       account_one_ending_balance,
    //       account_one_starting_balance - amount,
    //       "Amount wasn't correctly taken from the sender"
    //     );
    //     assert.equal(
    //       account_two_ending_balance,
    //       account_two_starting_balance + amount,
    //       "Amount wasn't correctly sent to the receiver"
    //     );
    //   });
});