// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/presets/ERC20PresetMinterPauserUpgradeable.sol";
/**
* @title PadelCoin
* @dev it is the coin itself. here I specify the name and the simbol
* 
 */
contract Paddy is Initializable, ERC20Upgradeable {
    
    event Bought(uint256 amount);

    // function initialize(string memory name, string memory symbol) public initializer{
    function initialize() public initializer{
        //create token and gives admin, minter, pauser to who deploys the contract
        __ERC20_init("PaddyCoin","PADDY"); 
    }

    receive() payable external {}

    function getPaddyBalance() public view returns(uint){
        return address(this).balance;
    }

    function buy() payable public {
        uint256 pay = msg.value;
        require(pay > 0, "You need to send some Ether");
        _mint(msg.sender,pay);
        emit Bought(pay);
    }

     
}
